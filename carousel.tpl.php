<ul id="brieftabs">
  <?php
    $list = array();
    foreach($proposals as $p)
      $list[] = l($p->title, '', array('fragment' => $p->id, 'attributes' => array('rel' => $p->id)));
    print theme_item_list($list, NULL, 'ul', array('class' => 'tab_list'));
  ?>
</ul>
<div id="brieftexts">
  <?php
    foreach($proposals as $p)
      print theme('carousel_item', $p);
  ?>
  <p><?php print $footer; ?></p>
</div>
