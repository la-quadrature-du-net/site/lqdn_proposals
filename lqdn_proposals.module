<?php

function _lqdn_proposals() {
  drupal_add_css(drupal_get_path('module', 'lqdn_proposals') . '/lqdn_proposals.css');
  drupal_add_js(drupal_get_path('module', 'lqdn_proposals') . '/lqdn_proposals.js');

  $proposals = array();
  $proposals[] = (object)array(
    'id' => 'human-rights',
    'title' => "Les droits de l'Homme dans la société numérique",
    'content' => "La liberté de communication et d'expression, le respect de la vie privée, le droit au procès équitable et les principes de l'État de droit tels que la séparation des pouvoirs sont des valeurs cardinales pour des sociétés libres. C'est pour les protéger que La Quadrature du Net s'oppose à la régulation extra-judiciaire des contenus en ligne.",
  );
  $proposals[] = (object)array(
    'id' => 'access',
    'title' => "L'accès à un Internet libre",
    'content' => "Internet s'est développé selon un modèle décentralisé, fondé sur le principe de neutralité du réseau. C'est sur cette architecture que repose la liberté de communication et l'innovation permise par Internet. Elle doit être défendue, notamment face aux velléités des opérateur télécoms, y compris dans les nouveaux territoires de l'internet mobile.",
  );
  $proposals[] = (object)array(
    'id' => 'sharing',
    'title' => "Le partage de la culture et des connaissances",
    'content' => "Internet et les technologies numériques permettent à chacun de partager librement l'information numérique. Cet immense progrès doit être défendu en mettant fin à l'absurde guerre contre le partage de la culture, symbolisée par l'HADOPI ou l'ACTA, et en créant les conditions d'un financement pérenne de la création.",
  );
  $footer = "Retrouvez les propositions de La Quadrature... www.laquadrature.net/fr/propositions";
  return theme('carousel', $proposals, $footer);
}

/**
 * Implementation of hook_block_view().
 */
function lqdn_proposals_block_view($delta = '') {
    $block = array();
    $block['subject'] = 'La Quadrature défend :';
    $block['content'] = _lqdn_proposals();
    return $block;
}

/**
 * Implementation of hook_block_list_alter().
 */
function lqdn_proposals_block_list_alter(&$blocks) {
    $blocks[0]['info'] = t('Proposals');
}

/**
 * Implementation of hook_theme().
 */
function lqdn_proposals_theme() {
  return array(
    'carousel' => array(
      'template' => 'carousel',
      'variables' => array(
        'proposals' => NULL,
        'footer' => NULL,
      ),
    ),
    'carousel_item' => array(
      'template' => 'carousel-item',
      'variables' => array(
        'proposal' => NULL,
      ),
    ),
  );
}
