// $Id$

jQuery.fn.valign = function() {
    $(this)
        .wrapInner('<div class="valign-box"></div>')
        .css("position", "relative");

    xH = $(this).children('.valign-box').height();
    $(this).children('.valign-box')
        .css({
            "position"    : "absolute",
            "height"    : xH,
            "top"     : "50%",
            "margin-top"  : 0-(xH/2)
        });
};

var rotationDossiersActive = true;
function rotationDossiers(i) {
    if( !rotationDossiersActive ) return;
    // Cacher la boite de contenu
    $("#brieftexts div:nth-child(" + i + ")").fadeOut();
    // Mettre l'onglet cliqué actif à la place du précédent
    $("#brieftabs .active").removeClass("active");
    i++;
    if( i == 4 ) i = 1; // si on dépasse le nombre d'éléments, on revient au premier
    $("#brieftabs li:nth-child(" + i + ")").addClass("active");
    // Montrer la boite demandée
    $("#brieftexts div:nth-child(" + i + ")").fadeIn();
    // Rappeler la fonction après un délai
    window.setTimeout( 'rotationDossiers(' + i + ');', 2000);
}

// Une fois le chargment de la page terminé ...
$(document).ready( function () {
    $("#brieftexts div.contentbrief").hide();
    $('#brieftexts').css('overflow','hidden');
    $("#brieftexts div:first").show();
    $("#brieftexts").css('height','inherit');
    $('#brief').addClass("withjs");
  
  // Rotation
    rotationDossiers(1);
  
  // Lorsqu'un onglet est cliqué
    $("#brieftabs li").click(function (e) {
        rotationDossiersActive = false;
        e.preventDefault();
    // Si l'onglet cliqué est celui actif, ne rien faire
        if ($(this).hasClass("active")) {
            return false;
        }
        var activ_brief = $($('#brieftabs .active a').attr('href'));
        var new_brief = $($(this).children('a').attr("href"));
        $('#brieftexts').height(Math.max($('#brieftexts').height(), new_brief.height()));
        new_brief.height(Math.max(activ_brief.height(), new_brief.height()));

    // Cacher la boite de contenu
        activ_brief.fadeOut();

    // Mettre l'onglet cliqué actif à la place du précédent
        $("#brieftabs .active").removeClass("active");
        $(this).addClass("active");

    // Montrer la boite demandée
        new_brief.fadeIn();

        return false;
    });

});
